"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var mainscene_1 = require("./scenes/mainscene");
var socket_sharer_1 = require("../shared/socket-sharer");
var Game = /** @class */ (function (_super) {
    __extends(Game, _super);
    function Game() {
        var _this = this;
        //because calling super() generates a new game instance and serves it to the client in phaser3, and we dont want 2 games
        if (1 < 0) {
            _this = _super.call(this) || this;
        }
        //dont pass with constructor because of how it gets converted to js (would cause error)
        _this.socketShare = new socket_sharer_1.SocketShare();
        _this.socketShare.socket = io.connect();
        _this.game = new Phaser.Game({
            width: 800,
            height: 600,
            type: Phaser.AUTO,
            parent: "maingame",
            scene: mainscene_1.Mainscene
        });
        return _this;
    }
    return Game;
}(Phaser.Game));
exports.Game = Game;
//# sourceMappingURL=game.js.map