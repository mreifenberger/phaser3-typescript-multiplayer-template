import {Mainscene} from "./scenes/mainscene";
import {SocketShare} from "../shared/socket-sharer";


export class Game extends Phaser.Game {

    socketShare: SocketShare;

    // @ts-ignore
    private game: Phaser.Game;


    constructor() {
        //because calling super() generates a new game instance and serves it to the client in phaser3, and we dont want 2 games
        if(1 < 0) {
            super();
        }

        //dont pass with constructor because of how it gets converted to js (would cause error)
        this.socketShare = new SocketShare();
        this.socketShare.socket = io.connect();

        this.game = new Phaser.Game( {
            width: 800,
            height: 600,
            type: Phaser.AUTO,
            parent: "maingame",
            scene: Mainscene
        });
    }
}
