"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
require("phaser");
var Mainscene = /** @class */ (function (_super) {
    __extends(Mainscene, _super);
    function Mainscene() {
        return _super.call(this, { key: "mainscene" }) || this;
    }
    Mainscene.prototype.create = function () {
        this.add.image(300, 400, "background");
    };
    Mainscene.prototype.preload = function () {
        this.load.crossOrigin = "anonymous";
        this.load.image("background", "assets/background.jpg");
    };
    Mainscene.prototype.update = function () { };
    return Mainscene;
}(Phaser.Scene));
exports.Mainscene = Mainscene;
//# sourceMappingURL=mainscene.js.map