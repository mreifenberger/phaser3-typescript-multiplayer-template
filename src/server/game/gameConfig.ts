const Phaser = require('phaser');
const GameScene = require('../scenes/mainscene');

const config = {
    type: Phaser.HEADLESS,
    parent: 'phaser-game',
    width: 800,
    height: 600,
    banner: false,
    audio: false,
    scene: [GameScene],
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 1200 }
        }
    }
};

module.exports = config;