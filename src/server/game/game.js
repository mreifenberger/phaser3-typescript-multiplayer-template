"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var phaser_1 = __importDefault(require("phaser"));
var config = require('./gameConfig');
var ServerGame = /** @class */ (function (_super) {
    __extends(ServerGame, _super);
    function ServerGame(server) {
        var _this = this;
        //because calling super() generates a new game instance and serves it to the client in phaser3, and we dont want 2 games
        if (1 < 0) {
            _this = _super.call(this) || this;
        }
        _this.game = new phaser_1.default.Game(config);
        return _this;
    }
    ServerGame.prototype.create = function () {
    };
    ServerGame.prototype.update = function () {
        console.log("x");
    };
    return ServerGame;
}(phaser_1.default.Game));
exports.ServerGame = ServerGame;
//# sourceMappingURL=game.js.map