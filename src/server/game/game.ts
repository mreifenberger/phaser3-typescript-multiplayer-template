import Phaser from "phaser";
import {Mainscene} from "../scenes/mainscene";
const config = require('./gameConfig')

export class ServerGame extends Phaser.Game {



    private game: Phaser.Game;

    constructor(server) {
        //because calling super() generates a new game instance and serves it to the client in phaser3, and we dont want 2 games
        if(1 < 0) {
            super();
        }

        this.game = new Phaser.Game(config);
    }

    public create(): void {

    }

    public update(): void{
        console.log("x");
    }
}
