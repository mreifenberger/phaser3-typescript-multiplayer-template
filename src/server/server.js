"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
Promise.resolve().then(function () { return __importStar(require('@geckos.io/phaser-on-nodejs')); });
var path = __importStar(require("path"));
var game_1 = require("./game/game");
var express = require('express');
var app = express();
var server = require('http').Server(app);
var cors = require('cors');
var game = new game_1.ServerGame(server);
app.use(cors());
//dieses io objekt an das servergame passen
var io = require('socket.io').listen(server); // it was require('socket.io')(server);
app.use(express.static('public'));
app.get('/', function (req, res) {
    res.sendFile(path.resolve('./index.html'));
});
// @ts-ignore
var serverGame;
var GameServer = /** @class */ (function () {
    function GameServer() {
        this.socketEvents();
        console.log("im constructor probsss");
        console.log(typeof (io));
    }
    GameServer.prototype.connect = function (port) {
        server.listen(port, function () {
            console.info("Listening on port " + port);
        });
    };
    GameServer.prototype.socketEvents = function () {
    };
    return GameServer;
}());
var gameSession = new GameServer();
gameSession.connect(3004);
//# sourceMappingURL=server.js.map