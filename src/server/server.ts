import('@geckos.io/phaser-on-nodejs')

// @ts-ignore
import { Socket } from "socket.io";
import { Request, Response } from "express";
import * as path from "path";
import {ServerGame} from "./game/game";


const express = require('express');
const app = express();
const server = require('http').Server(app);
const cors = require('cors');

const game = new ServerGame(server)

app.use(cors());

//dieses io objekt an das servergame passen
const io = require('socket.io').listen(server); // it was require('socket.io')(server);



app.use(express.static('public'));

app.get('/', function (req: Request, res: Response) {
    res.sendFile(path.resolve('./index.html'));
});


// @ts-ignore
let serverGame: ServerGame;



class GameServer {


    constructor() {
        this.socketEvents();
        console.log("im constructor probsss");
        console.log(typeof(io));
    }

    public connect(port: number): void {
        server.listen(port, () => {
            console.info(`Listening on port ${port}`);
        });
    }

    private socketEvents(): void {
    }

}

const gameSession = new GameServer();

gameSession.connect(3004);
